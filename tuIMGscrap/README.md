**tuIMGscrap** is a python script I wrote to download all the images of my blog. 

It's a very simple script that finds all the links to the images on your tumblr and
downloads it to a folder of your choosing. 

Requirements
============

You need python 2.7 to run this script. If you're on windows, you might want to 
download it [here](https://www.python.org/ftp/python/2.7/python-2.7.amd64.msi) .

If you're on other platforms take a look [here](https://www.python.org/download/releases/2.7/).
Use
===

After installation, open up a command line by typing 'cmd' into the search bar
at the windows start up menu and navigate to the folder where you downloaded the script.

The script takes two parameters, the name of your blog and the folder where you're 
saving your images. Simply use it like this:

``tuIMGscrap.py "myTumblrBlog" "C:\\Users\\MyAwesomeName\\tumblrBKP\\" ``

(Yeah double slashes are necessary if you're on Windows, and yeah end the path to the 
folder with double slashes as well).