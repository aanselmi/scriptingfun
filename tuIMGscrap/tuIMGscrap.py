'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''  tuIMGscrap - v.0.1                                                     ''
''  Download all the photos from your tumblr blog.                         ''
''  Uses urllib and BeautifulSoup.                                         ''
''  Warning: it's not universal as relies on tumblr layouts, so            ''
''  you should test it out first to see if it works with your layout.      ''
''  Written one sunday morning by aelur sadgod - last revision 13/09/2015  ''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

import sys
import requests
import bs4
import os
import urllib
import time 

blog_name = sys.argv[1]
dst_fldr = sys.argv[2]


def estaVacio(iterable):
	try:
		if iterable[0] == None:
			return True
	except:
		return True
	return False


i = 1
base_url = 'http://'+blog_name+'.tumblr.com/page/'
while (1):
	success = True 
	text = ""
	while success:
		try:
			response = requests.get(base_url + str(i))
			text = response.text
			success = False
		except Exception:
			time.sleep(2)
	
	soup = bs4.BeautifulSoup(text)
	photourls = soup.find_all('div', class_="photo-url")
	if estaVacio(photourls):
		photourls = soup.find_all('div', class_="photo")
	if estaVacio(photourls):
		photourls = soup.find_all('div', class_="photo post")
	if estaVacio(photourls):
		break
	print 'Scrapping ' + base_url + str(i) + '\n'
	index = 1
	for url in photourls:
		success = True 
		while success:
			imgurl = url.img
			imgurl = str(imgurl.get('src'))
			print '            ' + imgurl + '\n'
			ext = imgurl[imgurl.rindex('.'):len(imgurl)]
			try:
				urllib.urlretrieve(imgurl, dst_fldr+blog_name+"_page_"+str(i)+"_"+str(index)+ext)
			except Exception:
				time.sleep(2)
			success = False
		index += 1
	i += 1